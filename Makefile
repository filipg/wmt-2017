
MOSES_SCRIPTS=/opt/moses/moses-scripts
SRC=de
TRG=en
SYSTEM_DIR=wmt16_systems/$(SRC)-$(TRG)

all: dev-0/out.tsv dev-1/out.tsv test-A/out.tsv

%/out.tsv: %/out-pre.tsv
	sed 's/\@\@ //g' < $< | $(MOSES_SCRIPTS)/recaser/detruecase.perl > $@

%/out-pre.tsv: %/in-pre.tsv
	amun -m $(SYSTEM_DIR)/model.npz -s $(SYSTEM_DIR)/vocab.$(SRC).json -t $(SYSTEM_DIR)/vocab.$(TRG).json --mini-batch 50 --maxi-batch 1000 -b 12 -n --bpe $(SYSTEM_DIR)/$(SRC)$(TRG).bpe < $< > $@

%/in-pre.tsv: %/in.tsv wmt16_systems/$(SRC)-$(TRG)/truecase-model.de
	$(MOSES_SCRIPTS)/tokenizer/normalize-punctuation.perl -l $(SRC) < $< | \
	$(MOSES_SCRIPTS)/tokenizer/tokenizer.perl -l $(SRC) | \
	$(MOSES_SCRIPTS)/recaser/truecase.perl -model wmt16_systems/$(SRC)-$(TRG)/truecase-model.$(SRC) > $@

wmt16/wmt16_systems/$(SRC)-$(TRG)/model.npz wmt16_systems/$(SRC)-$(TRG)/truecase-model.$(SRC):
	wget -r -e robots=off -nH -np -R 'index.html*' http://data.statmt.org/wmt16_systems/$(SRC)-$(TRG)/
